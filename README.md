# README #

Philips Hue Broken Bulb Replacement Diffuser

### What is this repository for? ###

This repo is for work in progress for the 3D printable diffuser for broken Philips Hue bulbs.

### How do I get set up? ###

Download the .stl

Use Slic3r or your favorite slicing software to convert to G-code

load, run, and print on your 3D printer.

### Contribution guidelines ###

If you like, you can do a remix at http://www.thingiverse.com/thing:224455

### Who do I talk to? ###

-enjakobsen at gmail dot com
-the other users at http://www.thingiverse.com/thing:224455, who frankly have tried more materials than I have